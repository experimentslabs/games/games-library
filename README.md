# Game library

Compilation of minigames.

This is a work in progress; as the few minigames needs to be imported and ported in this repo.

## Games

- Printable games
  - Quatro: Sheets of quatro puzzles (french words)

## Usage:

```shell
yarn install
yarn dev

# Build for production
yarn build
```

