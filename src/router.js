import {createRouter, createWebHashHistory} from 'vue-router'
import Home from './Home.vue'

export default createRouter({
  routes: [
    {
      path: '/', name: 'home', component: Home,
    },
    {
      path: '/printable/quatro', name: 'quatro', component: () => import('./QuatroSheets.vue'),
    }
  ],
  history: createWebHashHistory(),
})
