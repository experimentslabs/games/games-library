import { createApp } from 'vue'
import router from './router'

import App from './App.vue'
import './stylesheets/app.scss'

createApp(App)
  .use(router)
  .mount('#app')
