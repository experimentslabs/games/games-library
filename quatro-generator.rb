#!/usr/bin/env ruby

# Creates a list of words between 6 and 12 characters
# Uses the "wfrench" dictionary
#    sudo apt install wfrench
require 'json'

list = File.read('/usr/share/dict/french')

# Additional excluded words
exclusions = %w[]

pairs = {}
final = {}

list.split("\n")
            .select{|line| [6, 9, 12].include? line.size}
            .reject{|line| exclusions.include? line}
            .map do |line|
              start = line.size/3
              letters = line[start...(start+start)]
              pairs[letters] ||= []
              pairs[letters].push [line[0...start], line[start+start...]]
            end
pairs.each_pair{ |key, values| final[key] = values if values.size > 2 }

File.write('src/words.json', final.to_json)
